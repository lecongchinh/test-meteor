const { Sequelize } = require('sequelize');
import config from '../config/config.json';

const db = config.development.database;
const username = config.development.username;
const password = config.development.password;
const host = config.development.host;
const dialect = config.development.dialect;
const sequelize = new Sequelize(db, username, password, {
    host: host,
    dialect: dialect
});

export default sequelize;