function setHeader(res) {
    return res.setHeader('Content-Type', 'application/json');
}

function successResponse(res, data = null, message = "") {
    let dataResponse = {
        "status": 1,
        "data": data,
        "message": message
    };

    return res.end(JSON.stringify(dataResponse))
}

function failResponse(res, message = "", error = "") {
    let dataResponse = {
        "status": 0,
        "message": message,
        "error": error
    }

    return res.end(JSON.stringify(dataResponse));
}

module.exports = {
    setHeader,
    successResponse,
    failResponse
}