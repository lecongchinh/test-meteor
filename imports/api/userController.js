import {setHeader, successResponse, failResponse} from './baseController'
import {createUserRequest} from '../startup/validate/createUserRequest';
import UserModel from '../../models/user'

const UserEloquentRepository = require('../startup/repositories/userEloquentRepository')

let userRepo = new UserEloquentRepository(UserModel);
let md5 = require('md5')

function getAll(res) {
    userRepo.getAll().then((users) => {
        setHeader(res);

        return successResponse(res, users, "Success!");
    }).catch((err) => {
        console.log(err)
        return failResponse(res, "Get all user failed!");
    });
}

async function store(req, res) {
    setHeader(res);
    let errors = createUserRequest(req.body)
    if (typeof errors != "undefined") {
        return failResponse(res, errors);
    }
    userRepo.findOne({email: req.body.email}).then((user) => {
        if ((typeof user != "undefined") && (user != null)) {
            return failResponse(res, "Email already exist!");
        }
    })

    let dataStore = {
        full_name: req.body.full_name,
        email: req.body.email,
        password: md5(req.body.password)
    }
    userRepo.store(dataStore).then((user) => {
        return successResponse(res, user);
    }).catch(() => {
        return failResponse(res, "Create user failed!")
    })
}

module.exports = {
    getAll,
    store
}