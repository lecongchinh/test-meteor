import {getAll, store} from '../../api/userController'

Router.route('users', {where: 'server'}).get(function () {
    getAll(this.response);
}).post(function () {
    store(this.request, this.response);
})