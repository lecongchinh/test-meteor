let validate = require('validate.js')

function createUserRequest(request) {
    let constraints = {
        full_name: {
            presence: true,
            length: {
                maximum: 255,
                minimum: 3,
            }
        },
        email: {
            presence: true,
            length: {
                minimum: 6,
                maximum: 255
            }
        },
        password: {
            presence: true,
            length: {
                minimum: 6,
                maximum: 255
            }
        }
    };

    let dataValidate = {
        full_name: request.full_name,
        email: request.email,
        password: request.password
    };

    return validate(dataValidate, constraints);
}

module.exports = {
    createUserRequest
}