import BaseRepository from './baseRepository'

class UserEloquentRepository extends BaseRepository {
    constructor(model) {
        super(model);
    }
}

module.exports = UserEloquentRepository;