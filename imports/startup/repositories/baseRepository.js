class BaseRepository {
    constructor(model) {
        this._model = model;
    }

    async getAll() {
        return await this._model.findAll();
    }

    async store(attribute = {}) {
        return await this._model.create(attribute);
    }

    async findOne(conditions = {}) {
        return await this._model.findOne({where: conditions})
    }

    async updateWhere(attributes = {}, conditions = {}) {
        return await this._model.update(attributes, {where: conditions})
    }

    async destroy(conditions = {}) {
        return await this._model.destroy(conditions);
    }
}

module.exports = BaseRepository;