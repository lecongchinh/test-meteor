'use strict';
require('mysql2')
const { DataTypes } = require("sequelize");
import sequelize from "../config/dbConnection";

const {
    Model
} = require('sequelize');

class User extends Model {
    static associate(models) {
        // define association here
    }
};

let UserInit = User.init({
    full_name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    email: {
        type: DataTypes.STRING(255),
        unique: true,
        allowNull: true
    },
    password: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
}, {
    sequelize,
    modelName: 'User',
    tableName: 'Users'
});

export default UserInit;